import {getPrice} from "./showData.js"
// We use just one function to rerender all the variables on screen, so that we don't have to
// consider which specific parts needs to be rerendered every time we update something.
// The performance here is not really relevant since we are only updating up to 4 things at any one time.
export function reRender(){
    document.getElementById("pay").innerHTML = pay+ " kr";
    document.getElementById("balance").innerHTML = balance + " kr";
    document.getElementById("loan-balance").innerHTML = loan + " kr";
    hideShowPayButton();

}
// Shows the pay back loan button if theres is an active loan and hides if not.
function hideShowPayButton(){
    let buttonElm = document.getElementById("pay-button");
    let balanceElm = document.getElementById("loan-balance");
    let wordElm = document.getElementById("loan-element");


    if(loan){
        buttonElm.style.display = "block";
        balanceElm.style.display = "block";
        wordElm.style.display = "block";

    } else{
        buttonElm.style.display = "none";
        balanceElm.style.display = "none";
        wordElm.style.display = "none";
    }
}


//variables for balance, pay and loan
// they are only defined in this file since it is not necessary to access them directly in "app.js"
let balance = 0;
let pay = 0;
let loan = 0;


// payback loan button that checks if it is possible to pay the entire loan or just a part of it.
export function handleClickPay(){
    if (pay>=loan){
        pay -= loan;
        loan = 0;
        balance += pay;
        pay = 0;

    } else {
        loan -= pay;
        pay = 0;
    }
    reRender();
    
}
// click handler for banking the money earned. With the logic for also paying 10% of an outstanding loan.
export function handleClickBank(){
    if(loan){
        if(loan> 0.1*pay){
            loan -= 0.1*pay;
            balance += 0.9*pay;
            pay = 0;
        } else {
            balance += pay-loan;
            loan = 0;
            pay = 0;
        }

    } else {
        balance += pay;
        pay = 0;
    }
    reRender();

}


export function handleClickWork(){
    pay +=100;
    reRender();

}

//checks if it is allowed to get a loan and prompts the user for how much they want to loan.
export function handleClickLoan(){
    let loanAmount;
    
    loanAmount = Number(prompt("How much do you want to loan?"));
    while(true){
        if (loan){
            loanAmount = Number(prompt("You already have an outstanding loan!"))
        } else if (isNaN(loanAmount) | loanAmount<0){
            loanAmount = Number(prompt("You can only loan a positive integer amount of money"));
        } else if(loanAmount> 2*balance){
            loanAmount = Number(prompt(`You are not allowed to loan more than ${2*balance} kr.`))
        } else {
            break;
        }
    }
    loan = loanAmount;
    balance += loan;
    reRender();
    
}



export function handleClickBuy(data){
    let price = getPrice(data);
    if(price<=balance){
        balance-= price;
        reRender();
        alert("Congratulations, you have just bought a new computer")        
    } else{
        alert("You don't have sufficient funds in your account to buy this computer.")
    }

}
