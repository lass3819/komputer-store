//Determines the currently selected laptop and returns the associated index of the "data" array.
function currentSelectionIndex(){
    let selectBox = document.getElementById("dropdown");
    let selectedValue = selectBox.options[selectBox.selectedIndex].value;
    //-1 so we get the index of the array
    return selectedValue-1;
}


// Renders the specs of the currently selected laptop.
// We use currentSelectionIndex() to get the index of the currently selected computer,
// and use this index to access the corresponding data from data.
function renderSpecsData(data){
    let IndexOfCurrentlySelected = currentSelectionIndex(); 
    let dropDownSpecs = document.getElementById("dropdown-specs");

    let specsList = document.createElement("ul");
    while(dropDownSpecs.lastElementChild){
        dropDownSpecs.removeChild(dropDownSpecs.lastElementChild);
        
    }
    dropDownSpecs.appendChild(specsList);    
     for (const spec of data[IndexOfCurrentlySelected].specs){

        let specsElement = document.createElement("li");
        let specsElementParagraph = document.createElement("p");

        specsElementParagraph.textContent = spec;
        specsElement.appendChild(specsElementParagraph)
        specsList.appendChild(specsElement);
    }
    
}

//loop through all the objects in the data array and add each as an "option" html-element.
function showDataInDropdown(data){
    for (const obj of data){
        let titleElement = document.createElement("option");
        titleElement.textContent = obj.title;
        titleElement.value = obj.id;
        document.getElementById("dropdown").appendChild(titleElement)        
        
    }
}

// this function use currentSelectionIndex() to get the index of the currently selected laptop,
// and accesses data to display each relevant part.
function showDataInLaptopSection(data){
    let IndexOfCurrentlySelected = currentSelectionIndex();
    //image
    let image = document.getElementById("description-image");
    image.src = `https://hickory-quilled-actress.glitch.me/${data[IndexOfCurrentlySelected].image}`;
    
    // description
    let descriptionTitle = document.getElementById("description-title");
    descriptionTitle.innerHTML = data[IndexOfCurrentlySelected].title;

    let descriptionParagraph = document.getElementById("description-paragraph");
    descriptionParagraph.innerHTML = data[IndexOfCurrentlySelected].description;

    //price and buy
    let descriptionBuy = document.getElementById("computer-price");
    descriptionBuy.innerHTML = data[IndexOfCurrentlySelected].price
}

// We give the dropdown menu an onchange property so that it will render the new data for both specs and description. 
export function showData(data){
    
    let dropDownId = document.getElementById("dropdown");
    dropDownId.onchange = ()=>{renderSpecsData(data); showDataInLaptopSection(data)};
    showDataInDropdown(data);
    renderSpecsData(data);
    showDataInLaptopSection(data)
}

export function getPrice(data){
    let IndexOfCurrentlySelected = currentSelectionIndex();
    let price = data[IndexOfCurrentlySelected].price;
    return price; 
}




