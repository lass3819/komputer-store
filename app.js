import getData from "./fetch.js"
import {handleClickBank,handleClickBuy,handleClickLoan,handleClickPay,handleClickWork} from "./clickHandlers.js"
import {showData} from "./showData.js";

document.body.onload = setup;





function setup(){
    

    // We use the getData function to get the data from the api and then execute showData which shows the relevant data for the computers on the site.
    // and then we start the eventListener where we need the data also.
    getData().then((data)=>{
        showData(data);
        document.getElementById("buy-button").addEventListener("click",()=>handleClickBuy(data));
    }).catch((error)=>{console.error(`error: ${error}`)});
    
    
    document.getElementById("loan-button").addEventListener("click",handleClickLoan);
    document.getElementById("bank-button").addEventListener("click",handleClickBank);
    document.getElementById("work-button").addEventListener("click",handleClickWork);
    document.getElementById("pay-button").addEventListener("click",handleClickPay);
    


}










